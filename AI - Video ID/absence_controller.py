import xlrd
import xlsxwriter
from datetime import datetime
from xlrd.sheet import ctype_text  

def getData():

    excel_file = None
    excel_file = "Absence-" + str(datetime.date(datetime.now())) + ".xlsx"
    

    xl_workbook = xlrd.open_workbook(excel_file)
    print("Workbook exists. Opening...")

    xl_sheet = xl_workbook.sheet_by_index(0)

    # Print 1st row values and types
    #
    student_list = []

    num_cols = xl_sheet.ncols   # Number of columns
    for row_idx in range(0, xl_sheet.nrows):    # Iterate through rows
        name = None
        status = None
        for col_idx in range(0, num_cols):  # Iterate through columns
            cell_obj = xl_sheet.cell(row_idx, col_idx)  # Get cell object by row, col
            if(col_idx == 0):
                name = cell_obj.value
            elif(col_idx == 1):
                status = cell_obj.value
        if(name != "Presence %"):
            student_list.append([name,status])
    # print(student_list)
    return student_list
            

    

def addPresence(studentName, presenceStatus):

    student_list = []
    try:
        student_list = getData()
        #edit list
        student_found = False

        for i in range(len(student_list)):
            if(student_list[i][0] == studentName):
                student_list[i][1] = presenceStatus
                student_found = True
                break

        if(student_found is False):
            student_list.append([studentName, presenceStatus])

        #Calculating presence %
        present = 0
        students_sum = len(student_list)
        for student in student_list:
            if student[1] == "Present":
                present += 1
        presence_percentage = (present / students_sum) * 100
        student_list.append(["Presence %", presence_percentage])

        writeData(student_list)
    except(FileNotFoundError):
        #Creating new
        print("Workbook doesn't exist. Creating new default...")
        writeData([])
        addPresence(studentName,presenceStatus)
        

def writeData(student_list):
    EXCEL_SHEET_NAME = "Absence-" + str(datetime.date(datetime.now())) + ".xlsx"
    workbook = xlsxwriter.Workbook(EXCEL_SHEET_NAME)
    worksheet = workbook.add_worksheet()

    if (len(student_list) <= 0):
        student_list = [
            ['Marko Stanojevic', 'Not present'],
            ['Mateja Cicic', 'Not present'],
            ['Simon Kovacevic', 'Not present'],
            ['Vladimir Bozanic', 'Not present']
        ]
        
    row = 0
    col = 0

    for student, presence in (student_list):
        worksheet.write(row, col,     student)
        worksheet.write(row, col + 1, presence)
        row += 1

    if (len(student_list) <= 0):
        worksheet.write(row, 0, 'Presence %')
        worksheet.write(row, 1, '0%')

    workbook.close()


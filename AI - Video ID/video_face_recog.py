import face_recognition
import os
import cv2
import pickle
import time
import dlib
from datetime import datetime
import absence_controller

#Defining constants
KNOWN_FACES_DIR = "known_faces"
TOLERANCE = 0.6
FRAME_SIZE = 3
FONT_SIZE = 2
MODEL = "hog" #conv. neural network || cnn
color = [0,255,0] #green
TRAINING = False

video = cv2.VideoCapture(0) #could put in a filename
print("Loading known faces...")
known_faces = [] #Encoded faces
known_names = [] #Names associated with the faces
added_students = []
for name in os.listdir(KNOWN_FACES_DIR):
    for filename in os.listdir(f"{KNOWN_FACES_DIR}/{name}"):
        encoding = pickle.load(open(f"{KNOWN_FACES_DIR}/{name}/{filename}","rb"))
        known_faces.append(encoding)
        known_names.append(str(name))

if len(known_names) > 0:
    next_id = str(datetime.now()).replace(".","").replace(":","").replace("-","").replace(" ","")
else:
    next_id = str(datetime.now()).replace(".","").replace(":","").replace("-","").replace(" ","")

print("Processing unknown faces...")
while True:
    ret, image = video.read()
    locations = face_recognition.face_locations(image, model=MODEL) #Coordinates of all faces
    encodings = face_recognition.face_encodings(image, locations)

    

    for face_encoding, face_location in zip(encodings, locations):
        results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE)
        match = None

        top_left = (face_location[3], face_location[0])
        bottom_right = (face_location[1], face_location[2])

        if True in results: #If a we have a single true
            match = known_names[results.index(True)]
            if match not in added_students and TRAINING == False and " " in match:
                print(f"Match found! Adding presence for: {match}")
                absence_controller.addPresence(match,"Present")
                added_students.append(match)
        else:
            if(TRAINING):
                match = str(next_id)
                next_id = str(datetime.now()).replace(".","").replace(":","").replace("-","").replace(" ","")
                known_names.append(match)
                known_faces.append(face_encoding)
                os.mkdir(f"{KNOWN_FACES_DIR}/{match}")
                pickle.dump(face_encoding, open(f"{KNOWN_FACES_DIR}/{match}/{match}-{int(time.time())}.pkl", "wb"))

        

        

        cv2.rectangle(image, top_left, bottom_right, color, FRAME_SIZE)

        #Text rectangleQ
        top_left = (face_location[3], face_location[2])
        bottom_right = (face_location[1], face_location[2]+22)
        cv2.rectangle(image, top_left, bottom_right, color, cv2.FILLED)
        cv2.putText(image, f"{match}", (face_location[3]+10, face_location[2]+15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200,200,200),FONT_SIZE)
        if match in added_students:
            cv2.putText(image, "registered", (face_location[3]+30, face_location[2]+45), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200,200,200),FONT_SIZE)

    cv2.imshow("", image)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        cv2.destroyWindow()



